import React from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import HomePage from "./pages/HomePage";
import LoginPage from "./pages/LoginPage";
import MyRecipes from "./pages/MyRecipes";
import SearchRecipes from "./pages/SearchRecipes";

const App = () => {
  return (
    <Router>
      <Routes>
        <Route exact path="/user/myrecipes" element={<MyRecipes />} />
        <Route exact path="" element={<LoginPage />} />
        <Route exact path="/user/search" element={<SearchRecipes />} />
        <Route exact path="user/homepage" element={<HomePage />} />
      </Routes>
    </Router>
  );
};

export default App;
