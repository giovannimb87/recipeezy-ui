import React from "react";
import { AppBar, Link } from "@mui/material";
import { Toolbar } from "@mui/material";
import { Typography } from "@mui/material";
import { green, orange } from "@mui/material/colors";
import { ThemeProvider, createTheme } from "@mui/material";
import { useNavigate } from "react-router-dom";

const theme = createTheme({
  palette: {
    primary: {
      main: green[500],
    },
    secondary: {
      main: orange[500],
    },
  },
});

const Navbar = ({ header }) => {
  const navigate = useNavigate();
  const routeChange = () => {
    let path = "/user/homepage";
    navigate(path);
  };

  return (
    <ThemeProvider theme={theme}>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" noWrap>
            {header}
          </Typography>
          
        </Toolbar>
      </AppBar>
    </ThemeProvider>
  );
};

export default Navbar;
