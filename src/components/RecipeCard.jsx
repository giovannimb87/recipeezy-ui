import * as React from 'react';
import { styled } from '@mui/material/styles';
import {Typography, Card, CardHeader, CardMedia, CardContent, CardActions, Collapse, Avatar, IconButton, Container} from '@mui/material';
import { green, orange } from '@mui/material/colors';
import DeleteIcon from '@mui/icons-material/Delete';
import ShareIcon from '@mui/icons-material/Share';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { SaveSharp } from '@mui/icons-material';
import { Edit } from '@mui/icons-material';
import { Box } from '@mui/system';
import imagedefault from "../assets/images/black-and-white-flower-dish-coloring-book-food-drawing-vegetable-plate-eating-png-clipart-thumbnail.jpg"

const ExpandMore = styled((props) => {
  const { expand, ...other } = props;
  return <IconButton {...other} />;
})(({ theme, expand }) => ({
  transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
  marginLeft: 'auto',
  transition: theme.transitions.create('transform', {
    duration: theme.transitions.duration.shortest,
  }),
}));

const RecipeCard = ({ recipe, deleteRecipe, saveRecipe, imageUri }) => {
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  return (
    <>
    <Card  raised={true} sx={{ opacity: .9, bgcolor: orange[400], width: 300, height: '100%', display: 'flex', flexDirection: 'column', mb: 2, ml: 1, mr: 1 }}>
      <CardHeader
        avatar={
          <Avatar sx={{ bgcolor: green[600] }} aria-label="recipe">
            R
          </Avatar>
        }
        
        action={
          <IconButton aria-label="share">
          <ShareIcon onClick={() => window.location.assign(recipe.sourceUrl)} />
        </IconButton>
        }
        title={recipe.title}
        
      />
      <CardMedia
        component="img"
        height="194"
        
        image={imageUri || imagedefault}
      />
      <CardContent>
        <Typography variant="body2" color="text.secondary">
        {recipe.description}
        </Typography>
      </CardContent>
      <CardActions disableSpacing>

        <IconButton onClick = {() => saveRecipe(recipe)} aria-label="add to favorites">
          <SaveSharp />
        </IconButton>
        <IconButton onClick={() => deleteRecipe(recipe.id)}>
            <DeleteIcon aria-label="delete"/>
        </IconButton>
        <IconButton onClick={() => deleteRecipe(recipe.id)}>
            <Edit aria-label="edit"/>
        </IconButton>

        <ExpandMore
          expand={expanded}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="show more"
        >
          <ExpandMoreIcon />
        </ExpandMore>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
          <Typography paragraph>Ingredients:</Typography>
          <Typography paragraph>
            {recipe.ingredients}
          </Typography>
          <Typography paragraph>Directions:</Typography>
          <Typography paragraph>
            {recipe.directions}
          </Typography>
        </CardContent>
      </Collapse>
    </Card>
    </>
  );
}

export default RecipeCard;