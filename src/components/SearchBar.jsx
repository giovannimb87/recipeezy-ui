import React from "react";
import { Input } from "@mui/material";
import { IconButton } from "@mui/material";
import { Search } from "@mui/icons-material";
import { green, orange } from "@mui/material/colors";
import { createTheme, ThemeProvider } from "@mui/material/styles";

const theme = createTheme({
  palette: {
    primary: {
      main: green[500],
    },
    secondary: {
      main: orange[500],
    },
  },
});

const SearchBar = ({ searchTextHandler, searchHandler, searchText }) => {
  return (
    <ThemeProvider theme={theme}>
      <form onSubmit={(e) => {
              searchHandler(e);
            }}>
        <Input
          value={searchText}
          placeholder="Search"
          onChange={(e) => {
            searchTextHandler(e);
          }}
          type="text"
        ></Input>
        <IconButton type="submit" color="secondary" size="large">
          <Search/>
        </IconButton>
      </form>
    </ThemeProvider>
  );
};

export default SearchBar;
