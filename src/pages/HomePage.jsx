import { Box, Button, Container, Grid, Stack, Typography } from '@mui/material'
import { createTheme, ThemeProvider } from "@mui/material/styles";
import {green, orange} from '@mui/material/colors'
import React from 'react'
import Navbar from '../components/Layout/Navbar'

const HomePage = () => {

  const theme = createTheme({
    palette: {
      primary: {
        main: green[500],
      },
      secondary: {
        main: orange[500],
      },
    },
  });
  return (
    <ThemeProvider theme={theme}>
    <Navbar header='Home'/>
    
 <Box
          sx={{
            backgroundRepeat: 'no-repeat',
            backgroundSize: "cover",
            backgroundPosition: "center",
            bgcolor: 'background.paper',
            pt: 12,
            backgroundImage: `url(https://www.blog.motifphotos.com/wp-content/uploads/2019/02/recipe-book-feature.jpg)`
            
          }}
        >
          <Container maxWidth="sm">
          <Box sx={{borderRadius: 8, border: '2px solid #ffa726', width: 'auto', opacity: .7}} bgcolor='white'>
            <Typography
              sx={{opacity: 1}}
              component="h1"
              variant="h3"
              align="center"
              gutterBottom
            >
              Be inspired.
            </Typography>
            
            <Typography sx={{color: `rgba(0,0,0, 1)`}} variant="h4" align="center" paragraph>
              In the childhood memories of every good cook, there's a large kitchen, a warm stove, and a simmering pot...
            </Typography>
            </Box>
            <Stack
              sx={{ pb: 3, pt: 4 }}
              direction="row"
              spacing={3}
              justifyContent="center"
            >
              <Button sx={{fontWeight: 'bold'}} fontWeight='bold' color='secondary' variant="contained">My Recipeez</Button>             
              <Button sx={{fontWeight: 'bold'}} variant="contained">Search Recipeez</Button>
            </Stack>
          </Container>
        </Box>
        <Container sx={{ py: 2 }} maxWidth="md">
  
          <Grid container spacing={4}>
           
          </Grid>
        </Container>
    </ThemeProvider>
  )
}

export default HomePage