import React from "react";
import { useEffect, useState } from "react";
import RecipeService from "../services/RecipeService";
import RecipeCard from "../components/RecipeCard";
import Grid from "@mui/material/Grid";
import { Container } from "@mui/material";
import { green } from "@mui/material/colors";
import Navbar from "../components/Layout/Navbar";

const MyRecipes = () => {

    const [recipes, setRecipes] = useState([]);

    useEffect(() => {
      loadRecipes();
    }, []);
  
    const loadRecipes = async () => {
      const res = await RecipeService.getAllRecipes();
      const data = await res.data;
      setRecipes(data);
    };

    const deleteRecipe = async (id) => {
        await RecipeService.deleteRecipe(id)
      }
    



  return (
    <>
    <Navbar header='My Recipeez'/>
    <Container sx={{pt: 10, alignText: 'center'}} fluid columns={3}>
          <Grid container direction="row" spacing={2}>
              {recipes.map((recipe) => (
                
          <RecipeCard key={recipe.id} deleteRecipe={deleteRecipe} recipe={recipe}/>
          
        ))}
        </Grid>
        </Container>
        </>
  )
};

export default MyRecipes;
