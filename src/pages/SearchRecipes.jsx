import React from "react";
import RecipeService from "../services/RecipeService";
import { useState, useEffect } from "react";
import SearchBar from "../components/SearchBar";
import RecipeCard from "../components/RecipeCard";
import Navbar from "../components/Layout/Navbar";
import { borderBottom, Box } from "@mui/system";
import { Container, Grid } from "@mui/material";

const SearchRecipes = () => {
  const [searchText, setSearchText] = useState("");
  const [results, setResults] = useState([]);
  const [imageURI, setImageURI] = useState('');

  const searchTextHandler = (e) => {
    setSearchText(e.target.value);
  };


  const searchHandler = async (e) => {
    e.preventDefault();
    const res = await RecipeService.searchRecipes(searchText);
    const data = res.data.results;
    setImageURI(res.data.baseUri)
    setResults(data);
  };

  const saveRecipe = async (recipe) =>  {
    const rec = {
      "name" : "Test",
      "description" : "Lorem ipsum dolor sit amet.",
      "ingredients" : "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
      "directions" : "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
      "image" : "https://toppng.com/uploads/preview/clipart-free-seaweed-clipart-draw-food-placeholder-11562968708qhzooxrjly.png"
    }
    await RecipeService.saveRecipe(rec);
  }

  return (
    <>
      <Navbar header="Search New Recipeez" />
      <Box sx={{borderBottom: '3px solid orange', width: '100%', height: '75px', textAlign: 'center'}}>
      <SearchBar
        searchText={searchText}
        searchHandler={searchHandler}
        searchTextHandler={searchTextHandler}
      ></SearchBar>
      </Box>
      <Container sx={{pt: 10, alignText: 'center'}} fluid columns={3}>
          <Grid container direction="row" spacing={2}>
      {results.length > 0 && results.map((result) => <RecipeCard imageUri={`${imageURI}${result.image}`} saveRecipe={saveRecipe} recipe={result} key={result.id}></RecipeCard>)}
      </Grid>
      </Container>
    </>
  );
};

export default SearchRecipes;
