import axios from 'axios';

const RECIPE_REST_BASE_URL = 'http://localhost:8081/recipes';

class RecipeService {

    getAllRecipes() {
        return axios.get(RECIPE_REST_BASE_URL)
    }

    saveRecipe(recipe) {
        return axios.post(`${RECIPE_REST_BASE_URL}/add`, recipe)
    }

    deleteRecipe(id) {
        return axios.delete(`${RECIPE_REST_BASE_URL}/remove/${id}`)
    }

    searchRecipes(query) {
        return axios.get(`${process.env.REACT_APP_SPNCLR_SEARCH}${query}`)
    }
}

export default new RecipeService();